import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


import {  Swipe, SwipeItem,Button,Lazyload,Badge , Sidebar, SidebarItem,
		Collapse, CollapseItem,Tab, Tabs ,Card,Image as VanImage,Tag ,Form ,
		Field,Notify,Toast,Cell, CellGroup,Col, Row, Checkbox, CheckboxGroup,Area ,
		Stepper,SubmitBar,SwipeCell,Empty,AddressList,AddressEdit,Icon,
		Divider,Dialog,Popup,Grid, GridItem,PullRefresh   
		} from 'vant';




createApp(App).
use(Swipe).
use(SwipeItem).
use(Lazyload,{
	loading:require('./assets/images/loading.gif')
}).
use(Badge).
use(Sidebar).
use(Collapse).
use(CollapseItem ).
use(SidebarItem).
use(Tab ).
use(Tabs).
use(Card).
use(Tag).
use(VanImage).
use(Button).
use(Form).
use(Field).
use(Notify).
use(Toast).
use(Cell).
use(CellGroup).
use(Col).
use(Icon).
use(Row).
use(Checkbox).
use(CheckboxGroup).
use(Stepper).
use(SubmitBar).
use(SwipeCell).
use(Empty).
use(Area ).
use(AddressList).
use(AddressEdit).
use(Divider).
use(Dialog).
use(Popup).
use(Grid).
use(GridItem).
use(PullRefresh).
	use(store).
	use(router).mount('#app')
