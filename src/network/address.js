import {request} from  './request';
//添加地址
export function  addAddress(data){
	return request({
		url:'/api/address',
		method:'post',
		data
	})
}

//修改地址
export function  editAddress(address,data){
	return request({
		url:`/api/address/${address}`,
		method:'PUT',
		data
	})
}

//地址列表
export function  listAddress(){
	return request({
		url:`/api/address`,
		method:'get',
	})
}

//删除地址
export function  deleteAddress(id){
	return request({
		url:`/api/address/${id}`,
		method:'DELETE'
	})
}

//地址详情
export function  detailAddress(address){
	return request({
		url:`/api/address/${address}`
	})
}

//设置为默认地址
export function  defaultAddress(address){
	return request({
		url:`/api/address/${address}/default`
	})
}

