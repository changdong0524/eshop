import {request} from  './request';

//添加购物车
export function  addCart(data){
	return request({
		url:'/api/carts',
		method:'post',
		data
	})
}


//修改购物车
//购物车数量改变 goodId 商品id num 数量
export function  editCart(goodId,data){
	return request({
		url:`/api/carts/${goodId}`,
		method:'PUT',
		data
	})
}


//购物车状态
export function  checkCart(data){
	return request({
		url:'/api/carts/checked',
		method:'PATCH',
		data
	})
}



//购物车列表
export function  listCart(data=''){
	return request({
		url:`/api/carts?${data}`
	})
}




//删除购物车
export function  deleteCart(id){
	return request({
		url:`/api/carts/${id}`,
		method:'DELETE'
	})
}
