import {request} from  './request';

//获取推荐数据
export function  getCategoryData(){
	return request({
		url:'/api/goods'
	})
}


//获取分类数据
export function  getCategoryGoods(order='sales',cid=0,page=1){
	return request({
		url:`/api/goods?category_id=${cid}&${order}=1&page=${page}`
	})
}
