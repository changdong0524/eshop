import {request} from  './request';

//获取推荐数据
export function  getHomeData(){
	return request({
		url:'/api/index'
	})
}


//获取选项卡数据
export function  getHomeGood(type='sales',page=0){
	return request({
		url:`/api/index?${type}=1&page=${page}`,
	})
}