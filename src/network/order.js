import {request} from  './request';


//订单预览
export function  viewOrder(){
	return request({
		url:'/api/orders/preview',
		method:'get',
	})
}

//创建订单
export function  createOrder(data){
	return request({
		url:'/api/orders',
		method:'post',
		data
	})
}


//订单详情
export function  detailOrder(order,str){
	return request({
		url:`/api/orders/${order}`,
		method:'get',
		params:{
			include:'user,orderDetail.goods'
		}
	})
}



//订单列表
export function  listOrder(order){
	return request({
		url:`/api/orders/${order}`,
		method:'get',
	})
}



//提交订单
export function  submitOrder(data){
	return request({
		url:'/api/orders/',
		method:'post',
		data
	})
}


//查询订单支付状态
export function  statusOrder(order){
	return request({
		url:`/api/orders/${order}/status`,
		method:'get',
	})
}

//获取支付二维码
export function  payCodeOrder(order,type){
	return request({
		url:`/api/orders/${order}/pay?type=${type}`,
		method:'get',
	})
}