import  axios  from  'axios'
import  router from '../router/index.js'
import { Toast } from 'vant';			
export  function  request(config){
	
	const  instance = axios.create({
		baseURL:"https://api.shop.eduwork.cn",
		timeout:5000
		
	})
	
	instance.interceptors.request.use(config=>{
		const  token   =  window.localStorage.getItem('token') || '';
		if(token){
			config.headers.Authorization = 'Bearer '+ token 
		}
		return  config
	},err=>{
		
	});
	
	instance.interceptors.response.use(res=>{
		
		return  res.data ? res.data:res
	},err=>{
		if(res.response.status  == 401){
			Toast.fail('没有登陆请先登陆哦')
			router.push({path:'/login'})
		}
		//console.log(err.response.data.errors[Object.keys(err.response.data.errors)][0]);
		Toast.fail(err.response.data.errors[Object.keys(err.response.data.errors)[0]][0])
	});
	
	return  instance(config)
}