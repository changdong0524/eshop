import {request} from  './request';

//注册
export function  userRegister(data){
	return request({
		url:'/api/auth/register',
		method:'post',
		data:data
	})
}


//登陆
export function  userLogin(data){
	return request({
		url:'/api/auth/login',
		method:'post',
		data:data
	})
}

//退出
export function  userLogout(){
	return request({
		url:'/api/auth/logout',
		method:'post'
	})
}


//用户信息
export function  getUser(){
	return request({
		url:'/api/user'
	})
}