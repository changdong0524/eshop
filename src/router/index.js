import { createRouter, createWebHistory } from 'vue-router'

const  Home  = ()=> import('../views/home/Home.vue')
const  Category  = ()=> import('../views/category/Category.vue')
const  ShopCart  =()=>  import('../views/shopcart/ShopCart.vue')
const  Profile  = ()=> import('../views/profile/Profile.vue')
const  Detail  =  ()=> import('../views/detail/Detail.vue')
const  Register  =  ()=> import('../views/profile/Register.vue')
const  Login  =  ()=> import('../views/profile/Login.vue')
const  Address  =  ()=> import('../views/address/Address.vue')

const  AddAddress  =  ()=> import('../views/address/AddEditAddress.vue')
const  CreateOrder  =  ()=> import('../views/createorder/createOrder.vue')
const  OrderDetail  =  ()=> import('../views/createorder/orderDetail.vue')
const  OrderList  =  ()=> import('../views/order/Order.vue')


const  Test  =  ()=> import('../views/test/test.vue')

import  store  from '../store';

const routes = [
	{
	  path: '',
	  name: 'Home',
	  component: Home,
	  meta:{
		  title:'首页'
	  }
	},
  {
    path: '/',
    name: 'Home',
    component: Home,
	meta:{
		title:'首页'
	}
  },
  {
    path: '/category',
    name: 'Category',
    component: Category,
	meta:{
		title:'商品分类'
	}
  },
  {
    path: '/shopcart',
    name: 'ShopCart',
    component: ShopCart,
	meta:{
		 title:'购物车',
		 isAuthRequired:true   //是否需要授权登陆才能访问
	}
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
	meta:{
		title:'个人中心',
		isAuthRequired:true //是否需要授权登陆才能访问
	}
  },
  {
    path: '/detail',
    name: 'Detail',
    component: Detail,
	meta:{
		title:'商品详情'
	}
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  	meta:{
  		title:'注册'
  	}
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  	meta:{
  		title:'登陆'
  	}
  },
  {
    path: '/createorder',
    name: 'createorder',
    component: CreateOrder,
  	meta:{
  		title:'订单'
  	}
  },
  {
    path: '/order',
    name: 'OrderDetail',
    component: OrderDetail,
  	meta:{
  		title:'订单详情'
  	}
  },
  {
    path: '/orderList',
    name: 'orderList',
    component: OrderList,
  	meta:{
  		title:'订单列表'
  	}
  },
  {
    path: '/address',
    name: 'address',
    component: Address,
  	meta:{
  		title:'地址'
  	}
  },
  {
    path: '/addAddress',
    name: 'addAddress',
    component: AddAddress,
  	meta:{
  		title:'新增地址'
  	}
  },
  {
    path: '/test',
    name: 'test',
    component: Test,
  	meta:{
  		title:'test'
  	}
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
	//如果没有登录(store.isLogin为false) 并且是需要登陆才能授权访问的页面    直接跳转到login
	if(to.meta.isAuthRequired && store.state.user.isLogin == false){
		//Toast.success('必须登陆后才能访问哦')
		alert('必须登陆后才能访问哦')
		return   next('/login')
	}else{
		next()
		document.title = to.meta.title
	}
	
})

export default router
