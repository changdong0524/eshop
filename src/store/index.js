import { createStore } from 'vuex'
import mutations  from  './mutations.js';
import actions  from  './actions.js';
import getters  from  './getters.js';

//存储用户是否已经登陆
const state={
	user:{
		isLogin:window.localStorage.getItem('token') ? true :false
	},
	cartCount:0
	
}


//这里是定义store
export default createStore({
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
})
